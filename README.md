# lib-bz2
Small library for compressing and decompressing to Bzip2

this lib can do these:

String -> bz2 byte[] (on fly) <br>
String -> .bz2 file <br>
byte[] -> bz2 byte[] (on fly) <br>
byte[] -> .bz2 file <br>
String -> compressed base64 <br>

string base64 -> uncompressed String <br>
byte[] -> uncompressed String <br>
byte[] -> uncompressed byte[] <br>


<invincibletrain@gmail.com>

Ed.
